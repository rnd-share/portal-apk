<?php
	header('Content-Type: application/json; charset=utf-8');
	
	include "vars.php";
    
    $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === 0 ? 'https://' : 'http://';
    $hostName = $_SERVER['HTTP_HOST'];

	$response = [];
	$response["raw_debug"]["POST"] = $_POST;
	$response["raw_debug"]["uploadedFile"] = $_FILES;
	$response["raw_debug"]["pathinfo"] = pathinfo($file["name"]);

	$file = $_FILES['uploadedFile'];
	$fileName = $_POST["fileName"];
	$appName = strtoupper($_POST["appName"]);
	$env = strtoupper($_POST["env"]);
	$version = $_POST["version"];
	
	
	if (!isset($file)){
        http_response_code(500);
		$response["status"] = "failed"; 
		$response["message"] = "No File has been chosen | Field uploadedFile is mandatory"; 
		echo json_encode($response);
        die;
    }
	
	
	if ($file['name'] == '') {
		http_response_code(500);
		$response["status"] = "failed"; 
		$response["message"] = "No File Has Been Uploaded | Field uploadedFile is mandatory";
		echo json_encode($response);
        die;
    }	
	
	if ($appName == '') {
		response_failed_mandatory_field("appName");
        die;
    }
	
	if ($env == '') {
        response_failed_mandatory_field("env");
        die;
    }
	
	if ($version == '') {
		response_failed_mandatory_field("version");
        die;
    }
	
	if ($fileName == '') {
		$path = pathinfo($file["name"]);
		$fileName = $path["basename"];
    }	
	

	chmod_r($dirPortal, 0777);	
	$dirFolder = $dirPortal. "/". $appName . "/". $env . "/". $version;
	
	
	mkdir($dirFolder, 0777, true);
	move_uploaded_file($file["tmp_name"], $dirFolder."/".$fileName);
	chmod_r($dirFolder, 0777);
	
	$response["folder"] = $dirFolder; 
	$response["fileName"] = $fileName; 


	http_response_code(200);	
	$response["status"] = "success"; 
	$response["message"] = $fileName. " has been uploaded"; 
	echo json_encode($response);
	
	
	function chmod_r($path) {
		$dir = new DirectoryIterator($path);
		foreach ($dir as $item) {
			chmod($item->getPathname(), 0777);
			if ($item->isDir() && !$item->isDot()) {
				chmod_r($item->getPathname());
			}
		}
	}
	
	function response_failed_mandatory_field($field_name) {
		http_response_code(500);
		$response["status"] = "failed"; 
		$response["message"] = "Field ".$field_name." is mandatory";
		echo json_encode($response);
	}

?>

