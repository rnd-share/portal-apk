<?php
    include "vars.php";

    $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === 0 ? 'https://' : 'http://';
    $hostName = $_SERVER['HTTP_HOST'];

    function getPathUrl() {
        if (isset($_SERVER['PATH_INFO'])) {
            $pathUrl = $_SERVER["PATH_INFO"];
            if ($pathUrl !== '' && $pathUrl !== '/') {
                return $pathUrl.'/';
            }
        }
        return '/';
    }
    $pathUrl = getPathUrl();
    $dirPortal = $dirPortal . $pathUrl;

    if (!isset($_FILES['uploadedFile'])){
        $Message = urlencode("No File has been chosen");
        header("Location:/.uploadIndex.php?Message=".$Message);
        die;
    }

    $f = $_FILES['uploadedFile'];

    if ($f['name'] == '') {
        $Message = urlencode("No File Has Been Uploaded");
        header("Location:/uploadIndex.php$pathUrl?Message=".$Message);
        die;
    }

    $file = $f['name'];
    $path = pathinfo($file);
    $filename = $path['filename'];
    $ext = $path['extension'];
    $temp_name = $f['tmp_name'];
    $filename = $filename.".".$ext;
    $path_filename_ext = $dirPortal.$filename;
    $msg = $filename. " has been uploaded";
    move_uploaded_file($temp_name,$path_filename_ext);

    $Message = urlencode($msg);
    http_response_code(302);
    header("Location:/index.php$pathUrl?Message=".$Message.'&highlight='.urlencode($filename));
?>