<?php
include "vars.php";

if (isset($_GET['highlight'])) {
	$highlight = $_GET['highlight'];
} else {
	$highlight = '';
}

$protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === 0 ? 'https://' : 'http://';
$hostName = $_SERVER['HTTP_HOST'];

function getPathUrl() {
	if (isset($_SERVER['PATH_INFO'])) {
		$pathUrl = $_SERVER["PATH_INFO"];
		if ($pathUrl !== '' && $pathUrl !== '/') {
			return $pathUrl.'/';
		}
	}
	return '/';
}
$pathUrl = getPathUrl();


if($pathUrl) {
	// if($pathUrl == "/") {
	// 	$pathUrl = "/";
	// } else {
	// 	$pathUrl = $pathUrl . "/";
	// }
	
	
	#echo "<br/><br/>TEST :: ". $dirPortal . $pathUrl;
	$dirPortal = $dirPortal . $pathUrl;
}

http_response_code(200);

?>
<!doctype html>
<html>
<head>
   <meta charset="UTF-8">
   <link rel="shortcut icon" href="./.favicon.ico">
   <title>PORTAL CLASSIC SOLUTION</title>

   <link rel="stylesheet" href="/.bootstrap.min.css">
   <link rel="stylesheet" href="/.style.css">
   <script src="/.sorttable.js"></script>
</head>

<body>
	<nav class="navbar navbar-expand navbar-dark bg-primary">
		<div class="container-fluid">
			<a class="navbar-brand" href="/">APK Portal</a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav me-auto mb-2 mb-lg-0">
				<li class="nav-item">
				<a class="nav-link active" aria-current="page" href="/index.php<?php echo $pathUrl; ?>">Download</a>
				</li>
				<li class="nav-item">
				<a class="nav-link" href="/.uploadIndex.php<?php echo $pathUrl; ?>">Upload</a>
				</li>
			</ul>
			</div>
		</div>
	</nav>

	<div id="content">
		<div id="">
			<?php
				if(isset($_GET['Message'])){
					echo "<h2 style='color:green; text-align: center; margin-top: 15px;'>" . $_GET['Message'] . "</h2>";
				}
			?>

		<h1> APK Portal </h1>
			
		<?php
			if ($pathUrl !== '' && $pathUrl != '/') {
				// echo '<a class="btn btn-light" href="/index.php/'. join('/', array_slice(array_filter(explode('/', $pathUrl)), 0, -1)) .'" style="background: url(./.images/file.png) no-repeat 10px 50%; "> 
				// Go to parent</a>';

				echo '<a class="btn btn-light btn-outline-dark" style="display: flex; width: 210px;" href="/index.php/'. join('/', array_slice(array_filter(explode('/', $pathUrl)), 0, -1)) .'"> 
					<div>
					<img src="/.images/return.png" style="height: 20px; margin-right: 8px;">
					</div>
					<div>
						Go to parent directory
					</div>
				</a>';
			}
		?>

			<table class="sortable">
				<thead style="background: #0275d8;">
					<tr>
						<th>Filename</th>
						<th>Type</th>
						<th>Size</th>
						<th>Date Modified</th>
						<th>Delete</th>
					</tr>
				</thead>
			<tbody>
				
				<?php
				
				// $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === 0 ? 'https://' : 'http://';
				// $hostName = $_SERVER['HTTP_HOST'];
				// $pathUrl  = strtok($_SERVER["REQUEST_URI"], '?');
				
				/*
				echo "<br/>1. ".$_SERVER['HTTP_HOST'];
				echo "<br/>2. ".$_SERVER['SERVER_NAME'];
				echo "<br/>3. ".$protocol;
				echo "<br/>4. ".$hostName;
				echo "<br/>5. ".$pathUrl;
				echo "<br/>6. ".$_SERVER["REQUEST_URI"];
				echo "<br/>7. ".isset($_GET['link']);
				echo "<br/>8. ".$_SERVER['QUERY_STRING'];;
				echo "<br/>9. ".$_SERVER['PATH_INFO']."<br/>";
				*/
				
				
				
				
				// $levelLocation = explode("/",$_SERVER['REQUEST_URI']);
				$levelLocation = explode("/", $pathUrl);
				
				/*
				echo "<br/>levelLocation : ".count($levelLocation)."<br/>";
				echo "REQUEST_URI :  ".$_SERVER['REQUEST_URI']."<br/><br/>";
				echo "pathUrl :  ".$pathUrl."<br/><br/>";
				*/
				
				
				
				$dirPortal = urldecode($dirPortal);
				
				/*
				echo "<br/>"; echo "<br/>";
				$files = scandir($dirPortal);
				$files = array_diff(scandir($dirPortal), array('.', '..'));
				foreach($files as $file){
				echo "<a href='$file'>$file</a>";
				}

				echo "<br/>";
				echo "<br/>";
				echo "<br/>";
				echo "<br/>";
				*/

			// Adds pretty filesizes
			function pretty_filesize($file) {
				$size=filesize($file);
				if($size<1024){$size=$size." Bytes";}
				elseif(($size<1048576)&&($size>1023)){$size=round($size/1024, 1)." KB";}
				elseif(($size<1073741824)&&($size>1048575)){$size=round($size/1048576, 1)." MB";}
				else{$size=round($size/1073741824, 1)." GB";}
				return $size;
			}

			// Checks to see if veiwing hidden files is enabled
			// if($_SERVER['QUERY_STRING']=="hidden") {
			if(isset($_GET['hidden'])) {
				$hide="";
				$ahref="./";
				$atext="Hide";
			} else {
				$hide=".";
				$ahref="./?hidden";
				$atext="Show";
			}

			// Opens directory
			$myDirectory=opendir($dirPortal);
			// Gets each entry
			while($entryName=readdir($myDirectory)) {
				$dirArray[]=$entryName;
			
				$modtime=date("M j Y g:i A", filemtime($dirPortal.$entryName));
				$timekey=date("YmdHis", filemtime($dirPortal.$entryName));
			}

			// Closes directory
			closedir($myDirectory);

			// Counts elements in array
			$indexCount=count($dirArray);

			// Sorts files
			//sort($dirArray);

			if($indexCount==2) {
				echo "<h2 style='color:red; text-align: center;'> There is no file in this directory </h2>";
			}

			// Loops through the array of files
			for($index=0; $index < $indexCount; $index++) {

				// Decides if hidden files should be displayed, based on query above.
				if(substr("$dirArray[$index]", 0, 1)!=$hide) {

					// Resets Variables
					$favicon="";
					$class="file";

					// Gets File Names
					$name=$dirArray[$index];
					$namehref=$dirArray[$index];

					// Gets Date Modified
					$modtime=date("M j Y g:i A", filemtime($dirPortal.$dirArray[$index]));
					$timekey=date("YmdHis", filemtime($dirPortal.$dirArray[$index]));


					// Separates directories, and performs operations on those directories
					if(is_dir($dirPortal.$dirArray[$index])) {
						$extn="&lt;Directory&gt;";
						$size="&lt;Directory&gt;";
						$sizekey="0";
						$class="dir";

						// Gets favicon.ico, and displays it, only if it exists.
						if(file_exists("$namehref/favicon.ico")) {
								$favicon=" style='background-image:url($namehref/favicon.ico);'";
								$extn="&lt;Website&gt;";
							}

						// Cleans up . and .. directories
						if($name=="."){
							$name=". (Current Directory)"; 
							$extn="&lt;System Dir&gt;"; 
							$favicon=" style='background-image:url($namehref/.favicon.ico);'";
						}
							
						if($name==".."){
							$name=".. (Parent Directory)"; 
							$extn="&lt;System Dir&gt;";
						}
					} else { // File-only operations
						
						// Gets file extension
						$extn=pathinfo($dirPortal.$dirArray[$index], PATHINFO_EXTENSION);

						// Prettifies file type
						switch ($extn){
							case "png": $extn="PNG Image"; break;
							case "jpg": $extn="JPEG Image"; break;
							case "jpeg": $extn="JPEG Image"; break;
							case "svg": $extn="SVG Image"; break;
							case "gif": $extn="GIF Image"; break;
							case "ico": $extn="Windows Icon"; break;

							case "txt": $extn="Text File"; break;
							case "log": $extn="Log File"; break;
							case "htm": $extn="HTML File"; break;
							case "html": $extn="HTML File"; break;
							case "xhtml": $extn="HTML File"; break;
							case "shtml": $extn="HTML File"; break;
							case "php": $extn="PHP Script"; break;
							case "js": $extn="Javascript File"; break;
							case "css": $extn="Stylesheet"; break;

							case "pdf": $extn="PDF Document"; break;
							case "xls": $extn="Spreadsheet"; break;
							case "xlsx": $extn="Spreadsheet"; break;
							case "doc": $extn="Microsoft Word Document"; break;
							case "docx": $extn="Microsoft Word Document"; break;

							case "zip": $extn="ZIP Archive"; break;
							case "htaccess": $extn="Apache Config File"; break;
							case "exe": $extn="Windows Executable"; break;

							default: 
								if($extn!=""){
									$extn=strtoupper($extn)." File";
								} else {
									$extn="Unknown"; 
								} 
							break;
							
						}

						// Gets and cleans up file size
						$size=pretty_filesize($dirPortal.$dirArray[$index]);
						$sizekey=filesize($dirPortal.$dirArray[$index]);
					}
				
					if($class == "dir") {
						// Output
						#<td><a href='$pathUrl/$namehref'$favicon class='name'>$name</a></td>
						// here
						// $curUri = "$protocol$hostName$pathUrl$namehref";
						$curUri = "/index.php$pathUrl$namehref";
						// var_dump($curUri);
						// die();
						#<a href="download.php?link=apk-crash-setoran-8.apk" download="w3logo">Download here<br/></a>
					} else {
						$link_encode = urlencode(urldecode("$pathUrl$namehref"));
						// $curUri = "$protocol$hostName";
						// var_dump($link_encode);
						// here
						// $curUri = "${curUri}/download.php?link=".base64_encode($link_encode);
						$curUri = "/download.php?link=".base64_encode($link_encode);
						// var_dump($curUri);
						// die();
						
					}
					
					echo("
					<tr class='$class" . ($name === $highlight ? ' highlight' : '') . "'>
						<td><a href='$curUri' class='name'>$name</a></td>
						<td><a href='./$namehref'>$extn</a></td>
						<td sorttable_customkey='$sizekey'><a href='./$namehref'>$size</a></td>
						<td sorttable_customkey='$timekey'><a href='./$namehref'>$modtime</a></td>
						
						<td>" . ($class==="dir" ? "" : "
					 		<form class='file-action-delete' method='POST' action='/.admin.php$pathUrl$namehref'>
					 		<input type='submit' value='Delete'>
							</form>") . "
						</td>
					 
					</tr>
						
					
					  
					 ");
				}
			}
			
			?>

				</tbody>
			</table>

			<h2 style="font-weight: bold; margin-top: 30px;">CLASSIC SOLUTION</h2>
		</div>
	</div>

	 <script>
		function deleteCallbackFn(ev) {
			ev.preventDefault();
			if (confirm('Do you want to delete?')) {
				ev.target.submit();
			}
		}
		document.querySelectorAll('.file-action-delete').forEach((el) => el.addEventListener('submit', deleteCallbackFn));
	</script>
</body>
</html>