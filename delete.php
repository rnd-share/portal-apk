<?php

    include "vars.php";

    $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === 0 ? 'https://' : 'http://';
    $hostName = $_SERVER['HTTP_HOST'];



    function getPathUrl() {
        if (isset($_SERVER['PATH_INFO'])) {
            $pathUrl = $_SERVER["PATH_INFO"];
            if ($pathUrl !== '' && $pathUrl !== '/') {
                return $pathUrl.'/';
            }
        }
        return '/';
    }

    $pathUrl = getPathUrl();
    $tempDir = $dirPortal.$pathUrl;
    $tempDir = substr_replace($tempDir,"", -1);
	
	if(is_file($tempDir)){
		unlink($tempDir);
	} else {
		deleteAll($tempDir);
	}
	
	
    $Message = urlencode(basename($pathUrl) . " Has Been Deleted");
    $newDir = join('/', array_slice(array_filter(explode('/', $pathUrl)), 0, -1));
    header("Location:/index.php/$newDir?Message=".$Message);


	function deleteAll($dir) {
		foreach(glob($dir . '/*') as $file) {
			if(is_dir($file)) {
				deleteAll($file);
			} else {
				unlink($file);
			}
		}
		rmdir($dir);
	}

?>