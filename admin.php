<?php
include "vars.php";

$protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === 0 ? 'https://' : 'http://';
$hostName = $_SERVER['HTTP_HOST'];

function getPathUrl() {
    if (isset($_SERVER['PATH_INFO'])) {
        $pathUrl = $_SERVER["PATH_INFO"];
        if ($pathUrl !== '' && $pathUrl !== '/') {
            return $pathUrl.'/';
        }
    }
    return '/';
}
$pathUrl = getPathUrl();
$dirPortal = null;
$dirPortal = $dirPortal . $pathUrl;

http_response_code(200);

?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="./.favicon.ico">
    <title>PORTAL CLASSIC SOLUTION</title>

    <link rel="stylesheet" href="/.bootstrap.min.css">
    <link rel="stylesheet" href="/.style.css">
    <script src="/.sorttable.js"></script>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container-fluid">
        <a class="navbar-brand" href="/">APK Portal</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="/">Download</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="/.uploadIndex.php<?php echo $pathUrl; ?>">Upload</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div id="content">
    <!-- <div id="container"> -->
    <h1>APK Portal</h1>

    <div class="card border-dark p-5 m-2">

        <form method="POST" action="/delete.php<?php echo $pathUrl; ?>" enctype="multipart/form-data">
            <h2 for="formFile" class="form-label" style="margin: 10px;">Input your credentials to delete "<?php echo basename($pathUrl)==="" ? "root folder" : basename($pathUrl) ?>" </h2>
            <div class="mb-3 mt-3">
                <label for="exampleInputEmail1" class="form-label">Username</label>
                <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="username">
            </div>
            <div class="mb-4">
                <label for="exampleInputPassword1" class="form-label">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" name="password">
            </div>

            <button type="submit" class="btn btn-primary">Delete File</button>
        </form>


        <?php
        if(isset($_GET['Message'])){
            echo "<p style='color:red; text-align: center;'>" . $_GET['Message'] . "</p>";
        }
        // unset($Message);
        ?>
    </div>
    <!-- </div> -->
</div>
</body>
</html>