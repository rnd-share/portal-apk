<?php
    include "vars.php";

    $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === 0 ? 'https://' : 'http://';
    $hostName = $_SERVER['HTTP_HOST'];



    function getPathUrl() {
        if (isset($_SERVER['PATH_INFO'])) {
            $pathUrl = $_SERVER["PATH_INFO"];
            if ($pathUrl !== '' && $pathUrl !== '/') {
                return $pathUrl.'/';
            }
        }
        return '/';
    }

    $pathUrl = getPathUrl();

    if($_POST['folderName']==""){
        http_response_code(302);
        $Message = urlencode("Folder Name is Still Empty");
        header("Location:/index.php$pathUrl?Message=".$Message);
        die();
    }


    $newDir = $dirPortal . $pathUrl . $_POST['folderName'];


    if(is_dir($newDir)){
        http_response_code(302);
        $Message = urlencode("Folder Already Exists!");
        header("Location:/index.php$pathUrl?Message=".$Message);
        die();
    }




    $tempDir = $pathUrl.$_POST['folderName'];
    mkdir($newDir, 0777, true);
    $Message = urlencode($_POST['folderName'] . " Folder Created");
    header("Location:/index.php$pathUrl?Message=".$Message.'&highlight='.urlencode($_POST['folderName']));



	function chmod_r($path) {
		$dir = new DirectoryIterator($path);
		foreach ($dir as $item) {
			chmod($item->getPathname(), 0777);
			if ($item->isDir() && !$item->isDot()) {
				chmod_r($item->getPathname());
			}
		}
	}

?>
